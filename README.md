# hisearch

## Overview

This tool is a quick script to `grep` over the cmd history.

It works best aliased in your `.bashrc`/similar shell as something like `hsch`.

## Usage

`./hisearch.sh [query]` where `[query]` is a regex query.
